extern crate sysfs_gpio;
extern crate spidev;

use spidev::{Spidev, SpidevOptions, SpidevTransfer, SPI_MODE_0};
use sysfs_gpio::{Direction, Pin};

use std::thread::sleep;
use std::time::Duration;

fn main() {
    let mut spi = match Spidev::open("/dev/spidev0.0") { // RPi
    // let mut spi = match Spidev::open("/dev/spidev1.0") { /// BBB
        Ok(ok) => ok,
        Err(e) => panic!("SPI: {}", e),
    };

    let options = SpidevOptions::new()
         .bits_per_word(8)
         .max_speed_hz(2_000_000)
         .mode(SPI_MODE_0)
         .lsb_first(false)
         .build();

    spi.configure(&options).expect("SPI: config");

    // 0->command 1->data
    let dc = Pin::new(25); //  RPi
    // let dc = Pin::new(30); //BBB
    dc.export().expect("export dc");
    dc.set_direction(Direction::Out).expect("set_direction dc");

    // reset signal input 0->active 1->inactive
    let rst = Pin::new(17); // RPi
    // let rst = Pin::new(60); //BBB
    rst.export().expect("export rst");
    rst.set_direction(Direction::Out).expect("set_direction rst");

    // busy signal 0->busy 1->not busy
    let busy = Pin::new(24); // RPi
    // let busy = Pin::new(31); //BBB
    busy.export().expect("export busy");
    busy.set_direction(Direction::In).expect("set_direction busy");

    println!("Busy:     {}", busy.get_value().expect("get_value busy"));
    println!("Reset:    {}", rst.get_value().expect("get_value reset"));
    println!("DC:       {}", dc.get_value().expect("get_value dc"));

    rst.set_value(0).expect("set_value rst");
    sleep(Duration::from_millis(2));
    rst.set_value(1).expect("set_value rst");
    sleep(Duration::from_millis(2));

    /////////////////////////////
    // POWER ON
    /////////////////////////////
        
    // set dc to command
    dc.set_value(0).expect("set_value dc");
    // create buffers for spi transfer
    let tx_buf = [0x04]; // power on
    let mut rx_buf = [0; 1];
    {
        let mut transfer = SpidevTransfer::read_write(&tx_buf, &mut rx_buf);
        spi.transfer(&mut transfer).unwrap();
    }
    println!("SPI: Sent:{:?} Recv:{:?}", tx_buf, rx_buf);

    while busy.get_value().unwrap() != 1 {
        sleep(Duration::from_millis(2));
    }
    println!("Finished being busy");

    /////////////////////////////
    // DISPLAY REFRESH
    /////////////////////////////

    // set dc to command
    dc.set_value(0).expect("set_value dc");
    // create buffers for spi transfer
    let tx_buf = [0x12]; // display refresh
    let mut rx_buf = [0; 1];
    {
        let mut transfer = SpidevTransfer::read_write(&tx_buf, &mut rx_buf);
        spi.transfer(&mut transfer).unwrap();
    }
    println!("SPI: Sent:{:?} Recv:{:?}", tx_buf, rx_buf);

    while busy.get_value().unwrap() != 1 {
        sleep(Duration::from_millis(2));
    }
    println!("Finished being busy");
}
