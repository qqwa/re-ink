#!/bin/bash

cargo build --release --target=armv7-unknown-linux-gnueabihf || exit
scp  target/armv7-unknown-linux-gnueabihf/release/re-ink beaglebone:~ || exit
ssh beaglebone -t -t './re-ink'

# config-pin P9.17 spi_cs
# config-pin P9.22 spi_sclk

# spi0_d1
# config-pin P9.18 spi

# spi0_d0
# config-pin P9.21 spi