#!/bin/bash

cargo build --release --target=armv7-unknown-linux-gnueabihf || exit
scp  target/armv7-unknown-linux-gnueabihf/release/re-ink raspberry:~ || exit
ssh raspberry -t -t './re-ink'
